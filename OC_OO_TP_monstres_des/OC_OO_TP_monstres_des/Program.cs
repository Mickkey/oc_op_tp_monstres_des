﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OC_OO_TP_monstres_des
{
    class Program
    {
        static void Main(string[] args)
        {
            int score = 0;
            Joueur mickkey = new Joueur();
            Random rand = new Random();

            while (mickkey.estVivant)
            {
                if (rand.Next(1,3) == 1)
                {
                    MonstreFacile monstre = new MonstreFacile();
                    while (monstre.estVivant)
                    {
                        if (!mickkey.estVivant)
                            break;
                        mickkey.Attaque(monstre);
                        monstre.Attaque(mickkey);
                    }
                    score++;
                }
                else
                {
                    MonstreDifficile monstre = new MonstreDifficile();
                    while (monstre.estVivant)
                    {
                        if (!mickkey.estVivant)
                            break;
                        mickkey.Attaque(monstre);
                        monstre.Attaque(mickkey);
                    }
                    score += 2;
                }

                
            }

            Console.WriteLine("Votre héro est mort :(");
            Console.WriteLine("Il a tué {0} monstres faciles", score);
            Console.ReadKey();
        }
    }
}
