﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OC_OO_TP_monstres_des
{
    class Joueur
    {
        public int vies { get; private set; }
        public bool estVivant;
        public De de { get; private set; }

        public Joueur()
        {
            vies = 150;
            estVivant = true;
            de = new De();
        }

        public void Attaque(MonstreFacile monstre)
        {
            if (LanceLeDe() >= monstre.LanceLeDe())
            {
                monstre.SubitDegats();
            }
        }

        public void SubitDegats(int degats)
        {
            if (de.LanceLeDe() >= 3) //Si le bouclier ne se déclenche pas
            {
                vies -= degats; //Le héro prend les dégats
            }
            if (vies < 1)
            {
                estVivant = false; //Si le héro n'a plus de vies, il est mort
            }
        }

        public int LanceLeDe()
        {
            return de.LanceLeDe();
        }
    }
}
