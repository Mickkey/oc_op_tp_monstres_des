﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OC_OO_TP_monstres_des
{
    class MonstreFacile
    {
        protected De de;
        private int degats;
        public bool estVivant { get; private set; }

        public MonstreFacile()
        {
            de = new De();
            degats = 10;
            estVivant = true;
        }

        public void Attaque(Joueur joueur)
        {
            if (LanceLeDe() > joueur.LanceLeDe())
            {
                joueur.SubitDegats(10);
            }
        }

        public void SubitDegats()
        {
            estVivant = false;
        }

        public int LanceLeDe()
        {
            return de.LanceLeDe();
        }
    }
}
