﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OC_OO_TP_monstres_des
{
    class De
    {
        private Random rand;

        public De()
        {
            rand = new Random();
        }

        public int LanceLeDe()
        {
            return rand.Next(1, 7);
        }
    }
}
